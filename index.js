// alert("Hi");

/*
	JSON Objects
		- JSON stands for javascript object notation.
		- JSON can also be used in other programming languages.
		- JS objects with JSON is different.
		- JSON is used for serializing different data types into bytes.
			-serialization: process of converting data types into series of bytes for easier transmission/transfer of information/
			-bytes: informations that a computer process to perform different task.

		- Uses double quotes for property name.
			let person = {
				"name" : "Yoon"
				"birthDay" : "March2, 2022"
			};	
*/

//JSON OBJECTS
// {
// 	"city" : "Quezon City", 
// 	"province" : "Metro Manila",
// 	"country" : "Philippines"
// }

// //JSON ARRAYS
// "cities" : [
// 	{
// 		"city" : "Quezon City",
// 		"province" : "Metro Manila",
// 		"country" : "Philippines"
// 	},
// 	{
// 		"city" : "Cebu City",
// 		"province" : "Cebu",
// 		"country" : "Philippines"
// 	}
// ];

// JSON METHODS
	//JSON Objects contain methods for parsing and converting data into stringfied JSON.

let batchesArr = [
	{batchName: "Batch 169"},
	{batchName: "Batch 170"}
];
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: "Luke",
	age: 45,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});
console.log(data);

// Using Stringify method with variables

//User details
let firstName = prompt("First Name:");
let lastName = prompt("Last Name:");
let age = prompt("Age:");
let address = {
	city: prompt("City:"),
	country: prompt("Country:")
};

let data2 = JSON.stringify({
	firstName : firstName,
	lastName : lastName,
	age : age,
	address : address
});

console.log(data2);

//Converting JSON into JS Objects

let batchesJSON = `[
	{
		"batchName" : "Batch 169"
	},
	{
		"batchName" : "Batch 170"
	}
]`
console.log(JSON.parse(batchesJSON));






